<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tree extends CI_Controller {



    /**
	 * Loads the user's dashboard
	 */
	public function buy_tree()
	{   
        
		
		if($this->session->has_userdata('user')){
            $data =array( 
                "species" => $this->Tree_model->getSpecies()
           );
                $this->load->view('common/head');
                $this->load->view('common/navbar');
                $this->load->view("tree/buy_tree",$data);
                $this->load->view('common/footer');
            
			
		} else {
			$this->session->set_flashdata('error', 'No ha iniciado sesión');
			redirect(site_url(['user','login']));
		}
    }
    /**
     * 
     */
	public function saveUserTree()
	{   
		$id_friend = $this->input->post("id_friend");
		$id_specie= $this->input->post("id_specie");
		$name = $this->input->post("name");
        $amount = $this->input->post("amount");
        $date=  date('Y-m-d');

		

		$data  = array(
			'id_friend' => $id_friend, 
			'id_specie' => $id_specie,
			'name' => $name,
            'amount' => $amount,
            'date' => $date
			
		);
		

		if ($this->Tree_model->save($data)) {
            $this->session->set_flashdata("exito","Registrada su compra");
			redirect(site_url(['user','dashboard']));
		}
		else{
			$this->session->set_flashdata("error","No se pudo guardar la informacion");
			redirect(site_url(['user','dashboard']));
		}
	}

	public function list_tree($id_user)
	{   
		if($this->session->has_userdata('user')){
		$data['trees'] = $this->Tree_model->getUserTrees($id_user);
		$this->load->view('common/head');
        $this->load->view('common/navbar');
        $this->load->view("tree/list_tree",$data);
		$this->load->view('common/footer');
	} else {
		$this->session->set_flashdata('error', 'No ha iniciado sesión');
		redirect(site_url(['user','login']));
	}

	}

	public function seeTree($id_tree)
	{   
		if($this->session->has_userdata('user')){
			$data =array( 
				"files" => $this->User_model->getPictures($id_tree),
				"tree" =>  $this->Tree_model->getTree($id_tree)
		   );
		   
		
		$this->load->view('common/head');
        $this->load->view('common/navbar');
        $this->load->view("tree/seeTree",$data);
		$this->load->view('common/footer');
		//Get files data from database
       
	} else {
		$this->session->set_flashdata('error', 'No ha iniciado sesión');
		redirect(site_url(['user','login']));
	}

	}

	
	

	
	
}
