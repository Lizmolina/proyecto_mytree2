<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
		parent::__construct();
		$this->load->model("User_model");
		$this->load->model("Tree_model");
	} 

	public function principal(){
		$this->load->view('common/head');
		$this->load->view('user/principal');
		
		$this->load->view('common/footer');
				
	}

	public function login()
	{
    // params
		// load data
		$this->load->view('user/login');
	
	}


	public function logout()
	{
		$this->session->sess_destroy();
    $this->session->set_flashdata('error', 'Inicie sesión nuevamente');
		redirect(site_url(['user','login']));
	}

	/**
	 * Loads the user's dashboard
	 */
	public function dashboard()
	{   
		$data['users'] = $this->User_model->allUserFriend();
		$data['usersQty'] = sizeof($data['users']);
		if($this->session->has_userdata('user')){
			$this->load->view('common/head');
			$this->load->view('common/navbar');
			$this->load->view('user/dashboard', $data);
			$this->load->view('common/footer');
			if($this->session->user->role == 'friend'){
				redirect(site_url(['user','buy_tree']));
				
			}
			
		} else {
			$this->session->set_flashdata('error', 'No ha iniciado sesión');
			redirect(site_url(['user','login']));
		}
	}
	public function buy_tree(){
		
		$data  = array(
			'species' => $this->Tree_model->getSpecies(), 
		);
		if($this->session->has_userdata('user')){
		$this->load->view('common/head');
		$this->load->view('common/navbar');
		$this->load->view("tree/buy_tree",$data);
		$this->load->view('common/footer');
	} else {
		$this->session->set_flashdata('error', 'No ha iniciado sesión');
		redirect(site_url(['user','login']));
	}
	}
	
	
	public function register()
	{   $this->load->view('common/head');
		$this->load->view('user/sing_in');
		$this->load->view('common/footer');
	}

	public function validation()
	{
		$validation_errors = [];
     
		if(!$this->input-post('name')){
			$validation_errors['name'] = 'is blank';
		}
		if(!$this->input-post('lastname')){
			$validation_errors['lastname'] = 'is blank';
		}
		if(!$this->input-post('phone')){
			$validation_errors['phone'] = 'is blank';
		}
		if(!$this->input-post('country')){
			$validation_errors['country'] = 'is blank';
		}
		if(!$this->input-post('username')){
			$validation_errors['username'] = 'is blank';
		}
		if(!$this->input-post('password')){
			$validation_errors['password'] = 'is blank';
		}
		if(!$this->input-post('address')){
			$validation_errors['address'] = 'is blank';
		}

		if(sizeof($validation_errors) < 0) {
			redirect(site_url(['user','saveUser']));
			
		}else{
			$this->session->set_flashdata('validation_errors', $validation_errors);
			redirect(site_url(['user','register']));
		}
	}
	public function saveUser()
	{   
		$name = $this->input->post("name");
		$lastname= $this->input->post("lastname");
		$phone = $this->input->post("phone");
		$country = $this->input->post("country");
		$username = $this->input->post("username");
		$password = $this->input->post("password");
		$address = $this->input->post("address");
		$role = $this->input->post("role");

		$data  = array(
			'name' => $name, 
			'lastname' => $lastname,
			'phone' => $phone,
			'country' => $country,
			'username' => $username,
			'password' => $password,
			'address' => $address,
			'role' => $role
		);
		

		if ($this->User_model->save($data)) {
			$this->session->set_userdata('user', $authUser);
			redirect(site_url(['user','login']));
		}
		else{
			$this->session->set_flashdata("error","No se pudo guardar la informacion");
			redirect(site_url(['user','register']));
		}
	}

	public function ver($id_user){
		$data =array( 
			"user" =>  $this->User_model->getById($id_user)[0],
			"trees" => $this->Tree_model->getUserTrees($id_user)
		);
		
		if($this->session->has_userdata('user')){
			if($this->session->user->role == 'admin'){
				$this->load->view('common/head');
			    $this->load->view('common/navbar');
		        $this->load->view('user/ver', $data);
			    $this->load->view('common/footer');
				
			}else{
				$this->session->set_flashdata('error', 'No cuenta con los permisos requeridos');
			}
			
		
			
		} else {
			$this->session->set_flashdata('error', 'No ha iniciado sesión');
			redirect(site_url(['user','login']));
		}
	}

	public function search($name = ''){
		$data['users'] = $this->User_model->getByName($name);
		$data['usersQty'] = sizeof($data['users']);
		$this->load->view('user/list', $data);
	}

	public function list()
	{
		$data['users'] = $this->User_model->allUserFriend();
		if($this->session->has_userdata('user')){
			if($this->session->user->role == 'admin'){
				$this->load->view('common/head');
			    $this->load->view('common/navbar');
		        $this->load->view('user/list', $data);
			    $this->load->view('common/footer');
				
			}else{
				$this->session->set_flashdata('error', 'No cuenta con los permisos requeridos');
			}
			
		
			
		} else {
			$this->session->set_flashdata('error', 'No ha iniciado sesión');
			redirect(site_url(['user','login']));
		}
		
	}

	

	public function editTree($id_tree)
	{
		$data =array( 
			"tree" => $this->Tree_model->getTree($id_tree),
			"species" => $this->Tree_model->getSpecies()
			
		);
		if($this->session->has_userdata('user')){
			if($this->session->user->role == 'admin'){
				$this->load->view('common/head');
			    $this->load->view('common/navbar');
		        $this->load->view('user/editTreeUser', $data);
			    $this->load->view('common/footer');
				
			}else{
				$this->session->set_flashdata('error', 'No cuenta con los permisos requeridos');
			}
			
		
			
		} else {
			$this->session->set_flashdata('error', 'No ha iniciado sesión');
			redirect(site_url(['user','login']));
		}
		
	}

	public function updatePicture(){
		$id_tree = $this->input->post("id_tree");
		$data = array();
        if($this->input->post('fileSubmit') && !empty($_FILES['userFiles']['name'])){
            $filesCount = count($_FILES['userFiles']['name']);
            for($i = 0; $i < $filesCount; $i++){
                $_FILES['userFile']['name'] = $_FILES['userFiles']['name'][$i];
                $_FILES['userFile']['type'] = $_FILES['userFiles']['type'][$i];
                $_FILES['userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'][$i];
                $_FILES['userFile']['error'] = $_FILES['userFiles']['error'][$i];
                $_FILES['userFile']['size'] = $_FILES['userFiles']['size'][$i];

                $uploadPath = 'img/files/';
                $config['upload_path'] = $uploadPath;
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size'] = '2048';
				$config['max_width'] = '2024';
				$config['max_height'] = '2008';
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('userFile')){
					$fileData = $this->upload->data();
					$uploadData[$i]['id_tree'] = $id_tree;
					$uploadData[$i]['file_name'] = $fileData['file_name'];
                    $uploadData[$i]['created'] = date("Y-m-d H:i:s");
                    $uploadData[$i]['modified'] = date("Y-m-d H:i:s");
                }
            }
            
            if(!empty($uploadData)){
                //Insert file information into the database
                $insert = $this->User_model->insert($uploadData);
                $statusMsg = $insert?'Files uploaded successfully.':'Some problem occurred, please try again.';
                $this->session->set_flashdata('statusMsg',$statusMsg);
            }
		}
		$this->updateTree();
	
		

        
    }
		
	


	public function updateTree(){
		$id_tree = $this->input->post("id_tree");
		$id_species= $this->input->post("id_specie");
		$height = $this->input->post("height");
		
		$data  = array(
			'id_tree' => $id_tree, 
			'id_specie' => $id_species,
			'height' => $height,
			
		);
		if ($this->Tree_model->update($id_tree,$data)) {
			redirect(base_url()."user/editTree/".$id_tree);
		}
		else{
			$this->session->set_flashdata("error","No se pudo guardar la informacion");
			redirect(base_url()."user/editTree/".$id_tree);
		}
	}

	

	/**
	 * Authenticates a user
	 */
	public function authenticate()
	{
		// get username and password
		$pass = $this->input->post('password');
		$user = $this->input->post('username');

		// check the database with that information
		$authUser = $this->User_model->authenticate($user, $pass);
		// return error or redirect to landing page
		if ($authUser) {
			$this->session->set_userdata('user', $authUser);
			redirect(site_url(['user','dashboard']));
		} else {
			$this->session->set_flashdata('error', 'Invalid username or password');
			redirect(site_url(['user','login']));
		}
	}
}
