<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tree_model extends CI_Model {

  
 
  /**
   *  Get all species from the databas
   *
   */
  public function getSpecies(){
    $query = $this->db->get('species');
    return $query->result();
  }

 /**
  * Save the information in the database
  */
  public function save($data){
		return $this->db->insert("trees",$data);
  }
  
  public function getUserTrees($id_user){
		$this->db->select("t.*,s.*");
		$this->db->from("trees t");
		$this->db->join("species s","t.id_specie = s.id_species");
		$this->db->where('id_friend', $id_user);
		$resultados = $this->db->get();
		return $resultados->result();
  }

  public function getTree($id_tree){
		$this->db->where("id_tree",$id_tree);
		$resultado = $this->db->get("trees");
		return $resultado->row();
  }
  
  public function update($id_tree,$data){
		$this->db->where("id_tree",$id_tree);
		return $this->db->update("trees",$data);
  }
  
  public function getTree1($id_tree){
		$this->db->where("id_tree",array('id_tree' => $id_tree));
		$resultado = $this->db->get("trees");
		return $resultado->row();
  }

}