
<nav class="navbar   navbar-expand-lg  bg-light ">
<a class="nav-link" href=#>
    <img src=# width="60" height="60" alt="">
  </a>
  
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link" href=#><h4>MyTree</h4>  
                <span class="sr-only">(current)</span>
              </a>
        </li>
        
        <li class="nav-item ">
          <a class="nav-link" href="<?php echo site_url(['user','dashboard']); ?> "><h4>Dashboard</h4> </a> 
        </li>
       

        <?php if($this->session->user->role == 'admin') { ?>
        <li class="nav-item ">
          <a class="nav-link" href="<?php echo site_url(['user','list']); ?> "><h4>See Friends</h4></a>
         
        </li>
        <?php }  ?>
        

        <?php if($this->session->user->role == 'friend') { ?>
        <li class="nav-item ">
          <a class="nav-link" href="<?php echo base_url()?>tree/list_tree/<?php echo $this->session->user->id_user; ?>"><h4>See Trees</h4> </a>
         
        </li>
        <?php }  ?>



       
      </ul>
    
     
      <ul class="nav justify-content-end">
      <li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"> <h4><?php echo $this->session->user->name ?></h4></a>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="<?php echo site_url(['user','logout']); ?> ">Logout</a>
      
      
    </div>
  </li>
       
        </ul>
    </div>
  
</nav>