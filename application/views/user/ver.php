<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container">
<div class="d-flex justify-content-center">
    <div class="msg">
    <?php echo $this->session->flashdata('error');?>
    </div>
    <h4 class="display-4">User Info</h4>
    </div>
    <div class="container">
    <div class="d-flex justify-content-center">
   
    <table class="table table-hover">

        <thead>
            <tr>
                <th>Name</th>
                <th>Username</th>
                <th>Phone</th>
                <th>Country</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
           <tr>
             <td><?php echo $user->name .' ' . $user->lastname  ?></td>
             <td><?php echo $user->username ?> </td>
             <td><?php echo $user->phone ?> </td>
             <td><?php echo $user->country ?> </td>
             <td>
             <button type="button" class="btn btn-info btn-view" data-toggle="modal" data-target="#modal-default" value="<?php echo $user->id_user;?>">Trees
            </td>
            </tr>
              
        </tbody>
    </table>
    
    </div>
    </div>
</div>
            
<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      <h4 class="modal-title">List Trees</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
       
      </div>
      <div class="modal-body">
      <table class="table table-hover">
        <thead>
            <tr>
                <th>Species</th>
                <th>Name</th>
                <th>Amount</th>
                <th>Date</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php if(!empty($trees)):?>
                <?php foreach($trees as $tree):?>
                    <tr>
                        <td><?php echo $tree->name_species;?></td>
                        <td><?php echo $tree->name;?></td>
                        <td><?php echo $tree->amount;?></td>
                        <td><?php echo $tree->date;?></td>
                        <td>
                            <a href="<?php echo base_url()?>user/editTree/<?php echo $tree->id_tree ?>" class="btn btn-primary btn-flat"><span class="fa fa-plus"></span>Edit</a>
                        </td>
                    </tr>
                    <?php endforeach;?>
                    <?php endif;?>
        </tbody>
    </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
