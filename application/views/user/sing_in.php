
<div class="container">
<div class=" text-center"  >
    
    <h1>Sing in</h1>
    <form action= "<?php echo site_url(['user','saveUser']) ?>" method="POST" class="form" role="form">
    <div class="msg">
     <?php echo $this->session->flashdata('error');?>
     </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="name">Name</label>
      <input type="text" class="form-control" id="name" name="name" placeholder="Name" require>
    </div>
    <div class="form-group col-md-6">
      <label for="lastname">Lastname</label>
      <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Lastname"  require>
    </div>
  </div>
  <div class="form-row">

  <div class="form-group col-md-6">
      <label for="phone_number">Phone Number</label>
      <input type="tel" class="form-control" id="phone" name="phone" placeholder="Phone Number" require>
    </div>
    <div class="form-group col-md-6">
      <label for="country">Country</label>
      <select id="country"  name="country" class="form-control">
        <option selected>Country</option>
        <option>Costa Rica</option>
        <option>Estado Unidos</option>
        <option>Salvador</option>
        <option>Honduras</option>
        <option>Nicaragura</option>
        <option>Guatemala</option>
        <option>Mexico</option>
        <option>Brazil</option>
        <option>Colombia</option>
        <option>Venezuela</option>
        <option>España</option>

      </select>
    </div>
   
    </div>
  <div class="form-row">
   
    <div class="form-group col-md-6">
      <label for="username">Email</label>
      <input type="email" class="form-control" id="username" name="username" placeholder="Email" require>
    </div>
    <div class="form-group col-md-6">
      <label for="password">Password</label>
      <input type="password" class="form-control" id="password" name="password" placeholder="Password" require>
    </div>
    </div>
   <input id="role" name="role" type="hidden" value="friend">
  
  
   
    <div class="form-group col-md-12">
    <label for="address">Address</label>
    <input type="text" class="form-control" id="address" name="address" placeholder="Address" require>
  </div>
    
 
    
    <div class="form-group col-md-12">
    
      <button type="submit" class="btn btn-primary">Sign in</button>
    </div>
</form>
  
  </div>
  </div>
