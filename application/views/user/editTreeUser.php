<div class="container">
<div class="d-flex justify-content-center">
   
    <h5 class="display-4">Edit user tree data</h5>
    </div>
    </div>

<div class="container">
<div class="d-flex justify-content-center">  

        <form enctype="multipart/form-data" action="<?php echo site_url(['user','updatePicture']) ?>" method="post">
        <p><?php echo $this->session->flashdata('statusMsg'); ?></p>
        <input type="hidden" name="id_tree" value="<?php echo $tree->id_tree;?>">

                            <div class="form-group row">
                            <label for="money" class="col-sm-3 col-form-label">Species:</label>
                            <div class="col-sm-9">
                                <select name="id_specie" id="id_specie" class="form-control">
                                    <?php foreach($species as $specie):?>
                                        <?php if($specie->id_species == $tree->id_specie):?>
                                        <option value="<?php echo $specie->id_species?>" selected><?php echo $specie->name_species;?></option>
                                    <?php else:?>
                                        <option value="<?php echo $specie->id_species?>"><?php echo $specie->name_species;?></option>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                </select>
                                </div>
                            </div>
                            <div class="form-group row ">
                            <label for="name" class="col-sm-3 col-form-label">Height:</label>
                                <div class="col-sm-9">
                                <input type="number" step="0.01" class="form-control" id="height" name="height" min="0"  value="<?php echo $tree->height?>">
                                </div>
                            </div>
        <div class="form-group  row ">
        <label for="name" class="col-sm-3 col-form-label">Pictures:</label>
                <div class="col-sm-9">
                <input type="file" class="form-control" name="userFiles[]" multiple/>
            </div>
            </div>
            <div class="form-group">
                <input class="btn btn-primary btn-flat btn-block pull-right" type="submit" name="fileSubmit" value="UPLOAD"/>
            </div>
        </form>

        
    </div>
   
</div>