<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/login.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    
    <title>Admin Login</title>
</head>
<body background="\dashboard\Web1\myTreePII\img\login.jpg" >
<div class="container">

  <div class="d-flex justify-content-center">
    <div class="card">
      <div class="card-header">
        <h2>Login</h2>
      </div>
      <div class="msg">
     <?php echo $this->session->flashdata('error');?>
     </div>
      <div class="card-body">
      <form class="form" action="<?php echo site_url(['user','authenticate']) ?>" method="POST" class="form-inline" role="form">
      <div class="form-group">
        <label class="sr-only" for="">Username</label>
        <input type="email" class="form-control" id="" name="username" placeholder="Your username">
      </div>
      <div class="form-group">
        <label class="sr-only" for="">Password</label>
        <input type="password" class="form-control" id="" name="password" placeholder="Your password">
      </div>

      <button  type="submit" class="btn btn-success btn-block">Login</button>
         
       </form>
     
      </div>
      <br>
      <br>
       
      <div class="card-footer">
      <a class="label label-dark" href="<?php echo site_url(['user','register']); ?>">Sing in</a>
      </div>
    </div>
  </div>

</html>

