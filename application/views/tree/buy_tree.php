<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
           
   
 <div class="container">
 <div class="d-flex justify-content-center">  
 <div class="msg">
     <?php echo $this->session->flashdata('error');?>
     </div>
              <div class="card border-success mb-3" style="width: 50rem;" >
                  <div class="card-header border-success mb-3">
                      <h3>Buy Tree</h3>
                  </div>
                  <div class="card-body">
                      <form action="<?php echo site_url(['tree','saveUserTree']) ?>" method="POST" class="form" role="form">
                      <input id="id_friend" name="id_friend" type="hidden" value="<?php echo $this->session->user->id_user ?>"> 

                      <div class="form-group row">  
                          <label for="money" class="col-sm-3 col-form-label">Species:</label>
                          <div class="col-sm-7">
                              <select class="form-control" name="id_specie" id="id_specie" require>
                                  <option selected>Select to Species</option>
                                  <?php foreach($species as $specie):?>
                                        <option value="<?php echo $specie->id_species?>"><?php echo $specie->name_species;?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            
                        </div>

                        <div class="form-group row ">
                            <label for="name" class="col-sm-3 col-form-label">Name:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="name"  name="name" placeholder="Name" require>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="money" class="col-sm-3 col-form-label">Donation:</label>
                            <div class="col-sm-7">
                                <input type="number" class="form-control" id="amount"  name="amount" placeholder="$$$$$" require>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="money" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-success btn-block pull-right">Buy</button>
                            </div>
                        </div>
                       </form>
                   </div>
                </div>


           
              
                </div>
        </div>
        </div>
      
        