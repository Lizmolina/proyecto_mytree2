<div class="container">
    <div class="d-flex justify-content-center">
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Species</th>
                <th>Name</th>
                <th>Amount</th>
                <th>Date</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php if(!empty($trees)):?>
                <?php foreach($trees as $tree):?>
                    <tr>
                        <td><?php echo $tree->name_species;?></td>
                        <td><?php echo $tree->name;?></td>
                        <td><?php echo $tree->amount;?></td>
                        <td><?php echo $tree->date;?></td>
                        <td>
                            <a href="<?php echo base_url()?>tree/seeTree/<?php echo $tree->id_tree ?>" class="btn btn-primary btn-flat"><span class="fa fa-plus"></span>Details</a>
                        </td>
                    </tr>
                    <?php endforeach;?>
                    <?php endif;?>
        </tbody>
    </table>
    </div>
</div>
            
